This is the implementation for the second coding challenge received from Aid.

It looks into alphabetically sorting search items from log input files that contain one search item per line. The input can be one or more
input file and it should handle up to 1 TB of data.
Also the implementation is limited to only having up to K search items in memory at a time and this item should be user configurable.

Assumptions:
- For simplicity I assume that the input files all use the default system encoding
- Conforming with the requirements when given an input folder we're looking only for ".log" files as input
- I assume that the memory requirements specifically refers to the number of items that we can keep in memory, so the small overhead of the different data structures is ignored

This implementation expects that the user should already have gradle 4.9 or later and Java 1.8 or later already installed and configured.

Build:

gradle build
(ran in the root folder of the project)

Running unit tests:

gradle test
(ran in the root folder of the project)

Running:

java -jar codingchallenge2-1.0-SNAPSHOT.jar {input_path} {output_path} [{max items in memory} {max opened files}{debug}]

Mandatory parameters:
    inputPath: Input location path. Can be a single file or a folder, will take all .txt files as input
    outputPath: Output location. Can be a folder or a file (if a folder it needs to be already created). If a folder it will be written to output.txt file
Optional parameters:
    maxItemsInMemory: The maximum number of search terms that can be storred in memory in a run. Defaults to 1024 if not set.
    max opened files: The maximum number of input files that will be opened at a time
    debug: boolean flag to enable printing debug statements
