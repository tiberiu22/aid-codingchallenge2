package com.aid.codingchallenge2;

import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.net.URL;

public class ExternalMergeSortTest {

    @Test
    public void testSingleFile() throws IOException {
        ClassLoader classLoader = ExternalMergeSortTest.class.getClassLoader();
        URL url = classLoader.getResource("file2.log");
        File input = new File(url.getFile());
        String[] args = {input.getPath(), input.getParent(), "2048"};
        App.main(args);

        File output = new File(classLoader.getResource("output.log").getFile());
        Assert.assertTrue(output.exists());
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(output)))) {
            String firstLine = reader.readLine();
            Assert.assertEquals("abacuses", firstLine);
        }

        // cleanup the output file, doing it like this since its easier to find the output file
        output.delete();
    }

    @Test
    public void testExampleInput() throws IOException {
        ClassLoader classLoader = ExternalMergeSortTest.class.getClassLoader();
        URL url = classLoader.getResource("example_input");
        File input = new File(url.getFile());
        String[] args = {input.getPath(), input.getParent()};
        App.main(args);

        File output = new File(classLoader.getResource("output.log").getFile());
        Assert.assertTrue(output.exists());
        File expectedOutput = new File(classLoader.getResource("example_input/expected_output.txt").getFile());
        // Test that the sorting is as expected and we have all the search items included
        try(BufferedReader outputReader = new BufferedReader(new InputStreamReader(new FileInputStream(output)));
            BufferedReader expectedReader = new BufferedReader(new InputStreamReader(new FileInputStream(expectedOutput)))) {
            boolean match = false;
            String outputLine;
            do {
                outputLine = outputReader.readLine();
                String expectedLine = expectedReader.readLine();
                // Skip empty lines
                if (outputLine == null) {
                    continue;
                }
                match = outputLine.equals(expectedLine);
            } while (outputLine != null);
            Assert.assertTrue(match);
        }

        // cleanup the output file, doing it like this since its easier to find the output file
        output.delete();
    }

    @Test
    public void testEmptyFile() throws IOException {
        ClassLoader classLoader = ExternalMergeSortTest.class.getClassLoader();
        URL url = classLoader.getResource("empty_input/empty_input1.log");
        File input = new File(url.getFile());
        String[] args = {input.getPath(), input.getParent()};
        App.main(args);

        File output = new File(classLoader.getResource("empty_input/output.log").getFile());
        Assert.assertTrue(output.exists());
        // Test that the output file doesn't contain any search items
        try(BufferedReader outputReader = new BufferedReader(new InputStreamReader(new FileInputStream(output)))) {
            boolean hasSearchItems = false;
            String line;
            do {
                line = outputReader.readLine();
                if (line != null && ! line.isEmpty()) {
                    hasSearchItems = true;
                }
            } while (line != null);
            Assert.assertFalse(hasSearchItems);
        }

        // cleanup the output file, doing it like this since its easier to find the output file
        output.delete();
    }

    @Test
    public void testMultipleEmptyFile() throws IOException {
        ClassLoader classLoader = ExternalMergeSortTest.class.getClassLoader();
        URL url = classLoader.getResource("empty_input");
        File input = new File(url.getFile());
        String[] args = {input.getPath(), input.getParent()};
        App.main(args);

        File output = new File(classLoader.getResource("output.log").getFile());
        Assert.assertTrue(output.exists());
        // Test that the output file doesn't contain any search items
        try(BufferedReader outputReader = new BufferedReader(new InputStreamReader(new FileInputStream(output)))) {
            boolean hasSearchItems = false;
            String line;
            do {
                line = outputReader.readLine();
                if (line != null && ! line.isEmpty()) {
                    hasSearchItems = true;
                }
            } while (line != null);
            Assert.assertFalse(hasSearchItems);
        }

        // cleanup the output file, doing it like this since its easier to find the output file
        output.delete();
    }

    @Test
    public void testInputFileWithEmptyLine() throws IOException {
        ClassLoader classLoader = ExternalMergeSortTest.class.getClassLoader();
        URL url = classLoader.getResource("empty_lines/file1.log");
        File input = new File(url.getFile());
        String[] args = {input.getPath(), input.getParent()};
        App.main(args);

        File output = new File(classLoader.getResource("empty_lines/output.log").getFile());
        Assert.assertTrue(output.exists());
        File expectedOutput = new File(classLoader.getResource("empty_lines/expectedOutputSingle.txt").getFile());
        // Test that the sorting is as expected and we have all the search items included
        try(BufferedReader outputReader = new BufferedReader(new InputStreamReader(new FileInputStream(output)));
            BufferedReader expectedReader = new BufferedReader(new InputStreamReader(new FileInputStream(expectedOutput)))) {
            boolean match = false;
            String outputLine;
            do {
                outputLine = outputReader.readLine();
                String expectedLine = expectedReader.readLine();
                // Skip empty lines
                if (outputLine == null) {
                    continue;
                }
                match = outputLine.equals(expectedLine);
            } while (outputLine != null);
            Assert.assertTrue(match);
        }

        // cleanup the output file, doing it like this since its easier to find the output file
        output.delete();
    }

    @Test
    public void testInputMultipleFilesWithEmptyLine() throws IOException {
        ClassLoader classLoader = ExternalMergeSortTest.class.getClassLoader();
        URL url = classLoader.getResource("empty_lines");
        File input = new File(url.getFile());
        String[] args = {input.getPath(), input.getParent()};
        App.main(args);

        File output = new File(classLoader.getResource("output.log").getFile());
        Assert.assertTrue(output.exists());
        File expectedOutput = new File(classLoader.getResource("empty_lines/expectedOutputMultiple.txt").getFile());
        // Test that the sorting is as expected and we have all the search items included
        try(BufferedReader outputReader = new BufferedReader(new InputStreamReader(new FileInputStream(output)));
            BufferedReader expectedReader = new BufferedReader(new InputStreamReader(new FileInputStream(expectedOutput)))) {
            boolean match = false;
            String outputLine;
            do {
                outputLine = outputReader.readLine();
                String expectedLine = expectedReader.readLine();
                // Skip empty lines
                if (outputLine == null) {
                    continue;
                }
                match = outputLine.equals(expectedLine);
            } while (outputLine != null);
            Assert.assertTrue(match);
        }

        // cleanup the output file, doing it like this since its easier to find the output file
        output.delete();
    }

}
