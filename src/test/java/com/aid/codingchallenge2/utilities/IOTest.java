package com.aid.codingchallenge2.utilities;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;

public class IOTest {

    @Test(expected = IOException.class)
    public void testNoInputExists() throws IOException {
        String nonExistentResource = "/src/test/resources/non_existing.log";
        new FileInput(nonExistentResource, null);
    }

    @Test(expected = IOException.class)
    public void testEmptyInputFolder() throws IOException {
        ClassLoader classLoader = IOTest.class.getClassLoader();
        URL url = classLoader.getResource("no_input");
        Assert.assertNotNull(url);
        new FileInput(url.getFile(), null);
    }
}
