package com.aid.codingchallenge2;

import com.aid.codingchallenge2.sort.ExternalMergeSort;
import com.aid.codingchallenge2.sort.SortAlgorithmInterface;
import com.aid.codingchallenge2.utilities.FileInput;
import com.aid.codingchallenge2.utilities.OutputUtil;

import java.io.File;
import java.io.IOException;

public class App {

    private static final Long MAX_ITEMS_IN_MEMORY = 1024L;

    private static SortAlgorithmInterface sortingAlgorithm = new ExternalMergeSort();

    public static void main(String[] args) throws IOException {
        Long startTime = System.currentTimeMillis();

        if (args.length < 2) {
            System.out.println(help());
            return;
        }
        // {input} {output} {max items in memory} {debug}
        String inputLocation = args[0];
        String outputLocation = args[1];
        Long maxItemsInMemory;
        Integer maxOpenedFiles;
        boolean debugEnabled;
        try {
            maxItemsInMemory = args.length > 2 ? Long.parseLong(args[2]) : MAX_ITEMS_IN_MEMORY;
            if (maxItemsInMemory < 0) {
                System.err.println("The given max items in memory should be positive.");
                return;
            }
            maxOpenedFiles = args.length > 3 ? Integer.parseInt(args[3]) : null;
            debugEnabled = args.length > 4 ? Boolean.parseBoolean(args[4]) : false;
            if (debugEnabled) {
                if (sortingAlgorithm instanceof ExternalMergeSort) {
                    ((ExternalMergeSort) sortingAlgorithm).setDebugEnabled(true);
                }
            }
        }
        catch (NumberFormatException e) {
            System.err.println("Max items in memory given isn't a number");
            throw e;
        }

        System.out.println("Output: " + outputLocation);
        System.out.println("Input: " + inputLocation);
        System.out.println("Max items in memory: " + maxItemsInMemory);

        File mergedFile = sortInput(inputLocation, outputLocation, maxItemsInMemory, maxOpenedFiles, debugEnabled);
        System.out.println("Created output in "
            + (mergedFile != null ? mergedFile.getAbsolutePath() : "n/a"));
        Long durationInMillis = System.currentTimeMillis() - startTime;
        System.out.println("Sorting took " + durationInMillis/1000.00 + " seconds");
    }

    private static File sortInput(String input, String output, Long maxItemsInMemory, Integer maxOpenedFiles, boolean debugEnabled) throws IOException {
        if (! OutputUtil.validateOutputLocation(output)) {
            throw new RuntimeException("Can not use this output location");
        }
        File outputFile = OutputUtil.getOutputFile(output);
        File outputDirectory = OutputUtil.getOutputDirectory(output);
        FileInput inputFile = new FileInput(input, maxOpenedFiles);
        inputFile.setDebugEnabled(debugEnabled);

        return sortingAlgorithm.sortSearchItems(inputFile, outputFile, outputDirectory, maxItemsInMemory);
    }

    private static String help() {
        return "Usage: " + System.lineSeparator()
            + "java -jar codingChallenge2 {inputPath} {outputPath} {maxItemsInMemory} {maxOpenedFiles} {debugEnabled}" + System.lineSeparator()
            + "Mandatory parameters: " + System.lineSeparator()
            + "inputPath: Input location path. Can be a single file or a folder, will take all .txt files as input" + System.lineSeparator()
            + "outputPath: Output location. Can be a folder or a file (if a folder it needs to be already created). If a folder it will be written to output.txt file" + System.lineSeparator()
            + "Optional parameters: " + System.lineSeparator()
            + "maxItemsInMemory: The maximum number of search terms that can be storred in memory in a run. Defaults to 1024 if not set." + System.lineSeparator()
            + "maxOpenedFiles: The maximum number of files that will be opened when reading the input." + System.lineSeparator()
            + "debugEnabled: boolean flag to enable printing debug statements";
    }
}
