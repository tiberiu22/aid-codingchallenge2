package com.aid.codingchallenge2.sort;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Caches the next line to be retrieved from a file and
 * wraps around a BufferedReader in order to read the entire file.
 *
 */
final class FileBuffer {

    public BufferedReader reader;
    private String cache;

    public FileBuffer(BufferedReader reader) throws IOException {
        this.reader = reader;
        reload();
    }
    public void close() throws IOException {

        this.reader.close();
    }

    public boolean isEmpty() {
        return this.cache == null;
    }

    public String peek() {
        return this.cache;
    }

    public String pop() throws IOException {
        String answer = peek();
        reload();
        return answer;
    }

    /**
     * Reads the next line from the file.
     *
     * @throws IOException
     */
    private void reload() throws IOException {
        this.cache = this.reader.readLine();
    }

}
