package com.aid.codingchallenge2.sort;

import com.aid.codingchallenge2.utilities.FileInput;

import java.io.File;
import java.io.IOException;

public interface SortAlgorithmInterface {

    /**
     * Alphabetically sorts search items from one or more input files with each
     * search item per line and saves the result in a file. With the limitation of
     * having a max number of search items in memory at a time.
     *
     * @param input input items
     * @param output output file
     * @param outputDirectory output directory
     * @param maxItemsInMemory max number of search items that can be stored in memory
     * @return output file with the sort items written to it
     */
    public File sortSearchItems(FileInput input, File output, File outputDirectory, Long maxItemsInMemory) throws IOException;

}
