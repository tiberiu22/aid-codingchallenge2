package com.aid.codingchallenge2.sort;

import com.aid.codingchallenge2.utilities.FileInput;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

/**
 * This class tries to implement a version of External Merge sort (as explained here: https://en.wikipedia.org/wiki/External_sorting).
 * This implementation takes into account that we can only have in memory at most K search items.
 * The steps of the algorithm are as follows:
 *
 * 1. We read up to K search items (lines) from our utilities
 * 2. We then sort these items and store them in a temporary file
 * 3. Redo steps 1-2 until we go through all items in the utilities
 * 4. Using the temporary files created previously we now have to merge sort these items.
 *  Due to the limitation of having only K items in memory the algorithm iterates over our temporary
 *  files in batches of K files and applies a form of the heapsort algorithm (described here: https://en.wikipedia.org/wiki/Heapsort).
 *  Each run on the batch will produce a single new temporary file, files which are collected and the algorithm
 *  iterates over them again until it produces a single merged sorted file in the output location.
 *
 * To implement this algorithm I looked at different sources such as:
 * https://github.com/lemire/externalsortinginjava
 * https://codereview.stackexchange.com/questions/60155/external-file-based-mergesort
 *
 */
public class ExternalMergeSort implements SortAlgorithmInterface {

    private Comparator<String> comparator;
    private boolean debugEnabled;

    public ExternalMergeSort() {
        this.comparator = Comparator.naturalOrder();
    }

    public ExternalMergeSort(Comparator<String> comparator) {
        this.comparator = comparator;
    }

    public void setDebugEnabled(boolean enabled) {
        this.debugEnabled = enabled;
    }

    @Override
    public File sortSearchItems(FileInput input, File output, File outputDirectory, Long maxItemsInMemory) throws IOException {
        List<File> temporaryFiles = batchSortInput(input, maxItemsInMemory, outputDirectory);
        output = mergeSortedFiles(temporaryFiles, outputDirectory, output, maxItemsInMemory);

        return output;
    }

    /**
     * Loads the data from the input files line by line and sorts the search terms into
     * multiple temporary sorted files that can be merged later.
     *
     * @param inputLocation location of input file
     * @param maxItemsInMemory max number of search terms that can be loaded in memory
     * @param tempDir location to write temporary files
     * @return list of temporary files
     */
    public List<File> batchSortInput(FileInput inputLocation, Long maxItemsInMemory, File tempDir) throws IOException {
        List<File> tempOutputFiles = new ArrayList<>();

        List<String> currentSearchTerms = new ArrayList<>();
        try (FileInput input = inputLocation){
            String line = "";
            while (line != null) {
                line = input.readLine();
                // Don't add empty lines to the search terms
                if (line == null || line.isEmpty() || line.equals(" ")) {
                    continue;
                }
                currentSearchTerms.add(line);
                if (currentSearchTerms.size() == maxItemsInMemory) {
                    tempOutputFiles.add(sortAndSave(currentSearchTerms, tempDir));
                    currentSearchTerms.clear();
                }
            }
        } finally {
            // Dump the remaining search terms if we finished reading all the input files but are still under the max block size
            if (! currentSearchTerms.isEmpty()) {
                tempOutputFiles.add(sortAndSave(currentSearchTerms, tempDir));
            }
            // If no output files -> probably empty input then create an empty temp file
            else if (tempOutputFiles.isEmpty()) {
                File newTempFile = File.createTempFile("sortFile", ".tmp", tempDir);
                newTempFile.deleteOnExit();
                tempOutputFiles.add(newTempFile);
            }
        }
        if (debugEnabled) {
            System.out.println("DEBUG: Created " + tempOutputFiles.size() + " temporary files");
        }

        return tempOutputFiles;
    }

    /**
     * Sorts the given list of search terms and saves that list in a temporary file.
     *
     * @param searchTerms to sort
     * @param outputLocation location where to store temporary file
     * @return temporary file
     * @throws IOException if we can not write to  the temporary file
     */
    private File sortAndSave(List<String> searchTerms, File outputLocation) throws IOException {
        searchTerms = searchTerms.parallelStream().sorted(Comparator.nullsLast(comparator)).collect(Collectors.toList());
        File newTempFile = File.createTempFile("sortFile", ".tmp", outputLocation);
        newTempFile.deleteOnExit();
        OutputStream outputStream = new FileOutputStream(newTempFile);
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream))) {
            for (String term: searchTerms) {
                writer.write(term);
                writer.newLine();
            }
        }
        if (debugEnabled) {
            System.out.println("DEBUG: Created " + newTempFile.getName() + " containing " + searchTerms.size() + " items");
        }
        return newTempFile;
    }

    /**
     * Merges the sorted temporary files and puts the result in the given output.
     *
     * @param files sorted temporary files to merge sort
     * @param tempDirectory directory in which to put the temporary files
     * @param outputLocation output file
     * @param maxItemsInMemory max number of items we can have in memory at a time
     * @return output file
     * @throws IOException
     */
    public File mergeSortedFiles(List<File> files, File tempDirectory, File outputLocation, Long maxItemsInMemory) throws IOException {
        if (files == null || files.isEmpty()) {
            return null;
        }
        if (files.size() == 1) {
            boolean success = files.get(0).renameTo(outputLocation);
            if (success) {
                return outputLocation;
            }
            else {
                throw new RuntimeException("Could not save merge sort in output file");
            }
        }

        if (debugEnabled) {
            System.out.println("DEBUG: Total number of temp files is " + files.size());
        }

        int index;
        List<File> mergedFiles = new ArrayList<>();
        List<File> tempFiles = files;
        while (tempFiles.size() != 1) {
            index = 0;
            /* Takes maxItemsInMemory files from our temporary files and
             * merges the files into a single temporary file, going over the entire
             * list of temporary files and collecting these new files in a list.
             * We then go over this list again and again until we get to a final single
             * output file which is our output file
            */
            while (index >= 0 && index < tempFiles.size()) {
                // iterate over our temp file list taking as many files as we can (up to maxItemsInMemory) per iteration
                int increment = (index + maxItemsInMemory) < tempFiles.size() ?
                    maxItemsInMemory.intValue() : tempFiles.size() - index;
                int endIndex = index + increment;
                // Merge this batch of temp files into 1 sorted temp file
                File mergedFile = mergeSortedFiles(tempFiles.subList(index, endIndex), tempDirectory);
                // Collect them in a list
                mergedFiles.add(mergedFile);
                // Move our index to the next location
                index = endIndex;
                if (debugEnabled) {
                    System.out.println("DEBUG: Merge iteration index is now at " + index + " and we have " + mergedFiles.size() + " merged files");
                }
            }
            tempFiles = new ArrayList<>(mergedFiles);
            mergedFiles.clear();
        }

        // Should have only 1 file at this moment
        // Rename the temp file to our output file
        boolean success = tempFiles.get(0).renameTo(outputLocation);
        if (success) {
            return outputLocation;
        }
        else {
            throw new RuntimeException("Could not save merge sort in output file");
        }
    }


    /**
     * Merges the sorted temporary files
     *
     * @param tempFiles to merge sort
     * @param tempDirectory where to write the temporary merged files
     * @throws IOException
     */
    private File mergeSortedFiles(List<File> tempFiles, File tempDirectory) throws IOException {
        /* Have a priority queue which contains a "buffered" version of our temporary
         * files which has pre-cached the next line in the file.
         * This will allow this priority queue to sort the items based on the next line.
         */
        PriorityQueue<FileBuffer> priorityQueue = new PriorityQueue<>(10,
            (FileBuffer file1, FileBuffer file2) ->  Comparator.nullsLast(comparator)
                .compare(file1.peek(), file2.peek())
        );
        // Convert the temp files to the buffered files and add them to the priority queue
        tempFiles.stream()
                .map(file -> {
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                        return new FileBuffer(reader);
                    } catch (IOException e) {
                        // Ignore files that error out and attempt to merge the remaining files
                        System.err.println("Something happened while trying to buffer file " + file.getPath());
                        e.printStackTrace();
                    }
                    return null;
                })
                // only look at files that are not isEmpty and ignore null values
                .filter(buffer -> buffer != null && ! buffer.isEmpty())
                .forEach(buffer -> priorityQueue.add(buffer));
        if (debugEnabled) {
            System.out.println("DEBUG: Attempting to merge " + priorityQueue.size() + " files");
        }

        File newTempFile = File.createTempFile("mergeSortFile", ".tmp", tempDirectory);
        newTempFile.deleteOnExit();
        OutputStream outputStream = new FileOutputStream(newTempFile);
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream))) {
            while (! priorityQueue.isEmpty()) {
                // Get the next string in the queue
                FileBuffer topBuffer = priorityQueue.poll();
                // Write it to our output file
                writer.write(topBuffer.pop());
                writer.newLine();
                // If the file is finished then we just close the buffer
                if (topBuffer.isEmpty()) {
                    topBuffer.close();
                }
                // Otherwise we add it back to the queue
                else {
                    priorityQueue.add(topBuffer);
                }
            }
        }
        finally {
            // Delete the old unnecessary temporary files now so we save a bit of space
            // could not do it as well since they will be deleted when the JVM exists correctly
            tempFiles.parallelStream().forEach(tempFile -> tempFile.delete());
        }
        return newTempFile;
    }

}
