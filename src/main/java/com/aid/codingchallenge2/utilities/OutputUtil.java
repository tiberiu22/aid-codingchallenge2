package com.aid.codingchallenge2.utilities;

import java.io.File;
import java.io.IOException;

/**
 * Utility class to handle working with the received output location.
 */
public class OutputUtil {

    private static final String OUTPUT_FILE_NAME = "output.log";

    /**
     * Validates that the output location can be used and the app has read/write access to it.
     *
     * @param outputLocation to check
     * @return true if output location is
     */
    public static boolean validateOutputLocation(String outputLocation) throws IOException {
        if (outputLocation == null || outputLocation.isEmpty()) {
            throw new RuntimeException("Output location is missing");
        }

        File outputFile = new File(outputLocation);
        if (! outputFile.canWrite() || ! outputFile.canRead()) {
            throw new IOException("Output location is not readable/writable");
        }

        return true;
    }

    public static File getOutputFile(String outputLocation) {
        File outputFile = new File(outputLocation);

        // If it refers to a directory then the output file needs to be {path}/output.txt
        if (outputFile.isDirectory()) {
            outputFile = new File(outputFile.getPath() + "/" + OUTPUT_FILE_NAME);
        }

        // If the output file already exists then we need to delete it so we can safely put our output there
        if (outputFile.exists()) {
            outputFile.delete();
        }

        return outputFile;
    }

    /**
     * Retrieves the directory in which the output file is located.
     *
     * @param outputLocation to get the directory from
     * @return output directory
     */
    public static File getOutputDirectory(String outputLocation) {
        File outputFile = new File(outputLocation);

        return outputFile.isDirectory() ? outputFile : outputFile.getParentFile();
    }
}
