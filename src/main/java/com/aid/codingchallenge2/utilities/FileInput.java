package com.aid.codingchallenge2.utilities;

import java.io.*;
import java.util.*;

/**
 * Wrapper class that abstracts away if we have 1 or more input files.
 * Takes as input files all the files with a .log extension in a folder.
 * It opens a max number of input files as to try not to reach the systems opened file limit.
 */
public class FileInput implements Closeable {

    private BufferedReader reader;
    private boolean debugEnabled;
    private List<File> inputFiles;
    private int index = 0;
    private int maxOpenedFiles = 2048;

    public FileInput(String inputLocation, Integer maxOpenedFiles) throws IOException {
        if (inputLocation == null || inputLocation.isEmpty()) {
            throw new IOException("Missing input location");
        }

        File file = new File(inputLocation);

        // Check if the file exists
        if (! file.exists()) {
            throw new IOException("Given input path doesn't exist!");
        }

        // Check if we can read from the file
        if (! file.canRead()) {
            throw new IOException("Can not read from input location!");
        }

        // Get all the files that we should look at
        List<File> inputFiles = new ArrayList<>();
        if (file.isDirectory()) {
            List<File> files = new ArrayList<>(
                Arrays.asList(file.listFiles((File currentFile) -> currentFile.getPath().contains(".log"))));
            inputFiles.addAll(files);
        }
        else {
            inputFiles.add(file);
        }

        if (debugEnabled) {
            System.out.println("We have " + inputFiles.size() + " input files");
        }

        if (inputFiles.isEmpty()) {
            throw new IOException("Could not find any input files!");
        }

        this.inputFiles = inputFiles;

        if (maxOpenedFiles != null) {
            this.maxOpenedFiles = maxOpenedFiles;
        }

        getNextBufferedReader();
    }

    /**
     * Opens the next batch of files and passes them to a buffered reader
     *
     * @throws IOException
     */
    private void getNextBufferedReader() throws IOException {
        // close previous reader and so other files
        if (reader != null) {
            if (debugEnabled) {
                System.out.println("DEBUG: Closing FileInput reader");
            }
            reader.close();
        }

        if (! hasFilesLeft()) {
            if (debugEnabled) {
                System.out.println("DEBUG: No more files left. Index at: " + index + " and file list size is " + inputFiles.size());
            }
            reader = null;
            return;
        }

        // Choose the next batch of files, up to maxOpenedFiles
        int increment = (index + maxOpenedFiles) < inputFiles.size() ? maxOpenedFiles : inputFiles.size() - index;
        int endIndex = index + increment;
        Vector<InputStream> vector = new Vector<>();
        for (File file : inputFiles.subList(index, endIndex)) {
            vector.add(new FileInputStream(file));
            // We need to add an extra line separator as the sequence input stream ignores EOF line separators
            vector.add(new ByteArrayInputStream(System.lineSeparator().getBytes()));
        }
        index = endIndex;
        if (debugEnabled) {
            System.out.println("DEBUG: Index at " + index + " with filelist size being " + inputFiles.size() + " current batch size is " + vector.size());
        }
        reader = new BufferedReader(new InputStreamReader(new SequenceInputStream(vector.elements())));
    }

    /**
     * Checks if we have any more files to look at.
     *
     * @return true if we have more files
     */
    private boolean hasFilesLeft() {
        // Check if we have any more files
        if (index >= inputFiles.size()) {
            return false;
        }

        return true;
    }

    /**
     * Read the next line from the utilities files.
     *
     * @return next line in the utilities or null if we reached the end of our utilities
     * @throws IOException If an I/O error occurs
     */
    public String readLine() throws IOException {
        String nextLine = reader.readLine();
        // Reached the end of the stream
        if (nextLine == null) {
            // Try and load up the next batch of files
            getNextBufferedReader();
            // If we have a reader now then read the next line
            if (reader != null) {
                nextLine = reader.readLine();
            }
        }

        return nextLine;
    }

    @Override
    public void close() throws IOException {
        // Only need to close the reader since it will close the opened files, the others should have not been opened yet
        if (reader != null) {
            reader.close();
        }
    }

    public void setDebugEnabled(boolean enabled) {
        this.debugEnabled = enabled;
    }
}
